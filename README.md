# Floriane Philippe

Floriane_Philippe est un projet de portfolio pour l'artiste du même nom.

## Enironnement de développement

### Pré-requis
* PHP 8.0.12
* Composer
* Symfony CLI
* Docker
* Docker Compose

Vous pouvez vérifier les pré-requis (sauf pour le Docker et Docker compose) en exécutant la commande suivante (ed la CLI Symfony):

``* bash : symfony check:requirements``

### Lancer l'environnement de développement

``* bash :	docker-compose up -d``
``* bash :	symfony serve -d``

## Lancer des test 
``*bash : php bin\phpunit --testdox``