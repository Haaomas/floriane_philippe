<?php

namespace App\Entity;

use App\Repository\CategorieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategorieRepository::class)
 */
class Categorie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\ManyToMany(targetEntity=Drawing::class, mappedBy="categorie")
     */
    private $drawings;

    public function __construct()
    {
        $this->drawings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|Drawing[]
     */
    public function getDrawings(): Collection
    {
        return $this->drawings;
    }

    public function addDrawing(Drawing $drawing): self
    {
        if (!$this->drawings->contains($drawing)) {
            $this->drawings[] = $drawing;
            $drawing->addCategorie($this);
        }

        return $this;
    }

    public function removeDrawing(Drawing $drawing): self
    {
        if ($this->drawings->removeElement($drawing)) {
            $drawing->removeCategorie($this);
        }

        return $this;
    }
}
