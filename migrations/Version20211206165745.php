<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211206165745 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE drawing_categorie (drawing_id INT NOT NULL, categorie_id INT NOT NULL, INDEX IDX_332263CCE6552D89 (drawing_id), INDEX IDX_332263CCBCF5E72D (categorie_id), PRIMARY KEY(drawing_id, categorie_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE drawing_categorie ADD CONSTRAINT FK_332263CCE6552D89 FOREIGN KEY (drawing_id) REFERENCES drawing (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE drawing_categorie ADD CONSTRAINT FK_332263CCBCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE drawing ADD user_id INT NOT NULL');
        $this->addSql('ALTER TABLE drawing ADD CONSTRAINT FK_996B9FE7A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_996B9FE7A76ED395 ON drawing (user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE drawing_categorie');
        $this->addSql('ALTER TABLE drawing DROP FOREIGN KEY FK_996B9FE7A76ED395');
        $this->addSql('DROP INDEX IDX_996B9FE7A76ED395 ON drawing');
        $this->addSql('ALTER TABLE drawing DROP user_id');
    }
}
