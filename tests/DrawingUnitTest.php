<?php

namespace App\Tests;

use App\Entity\Categorie;
use App\Entity\Drawing;
use App\Entity\User;
use DateTime;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class DrawingUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $drawing = new Drawing();
        $dateTime = new DateTime();
        $dateTimeImmutable = new DateTimeImmutable();
        $categorie = new Categorie();
        $user = new User();

        $drawing->setName('name')
            ->setWidth(20.20)
            ->setHeight(20.20)
            ->setSelling(true)
            ->setCreationDate($dateTime)
            ->setCreatedAt($dateTimeImmutable)
            ->setDescription('description')
            ->setPortfolio(true)
            ->setSlug('slug')
            ->setFile('file')
            ->addCategorie($categorie)
            ->setPrice(20.20)
            ->setUser($user);

        $this->assertTrue($drawing->getName() === 'name');
        $this->assertTrue($drawing->getWidth() == 20.20);
        $this->assertTrue($drawing->getHeight() == 20.20);
        $this->assertTrue($drawing->getSelling() === true);
        $this->assertTrue($drawing->getCreationDate() === $dateTime);
        $this->assertTrue($drawing->getCreatedAt() === $dateTimeImmutable);
        $this->assertTrue($drawing->getDescription() === 'description');
        $this->assertTrue($drawing->getPortfolio() === true);
        $this->assertTrue($drawing->getSlug() === 'slug');
        $this->assertTrue($drawing->getFile() === 'file');
        $this->assertContains($categorie, $drawing->getCategorie());
        $this->assertTrue($drawing->getPrice() == 20.20);
        $this->assertTrue($drawing->getUser() === $user);
    }

    public function testIsFalse()
    {
        $drawing = new Drawing();
        $dateTime = new DateTime();
        $dateTimeImmutable = new DateTimeImmutable();
        $categorie = new Categorie();
        $user = new User();

        $drawing->setName('name')
            ->setWidth(20.20)
            ->setHeight(20.20)
            ->setSelling(true)
            ->setCreationDate($dateTime)
            ->setCreatedAt($dateTimeImmutable)
            ->setDescription('description')
            ->setPortfolio(true)
            ->setSlug('slug')
            ->setFile('file')
            ->addCategorie($categorie)
            ->setPrice(20.20)
            ->setUser($user);

        $this->assertFalse($drawing->getName() === 'false');
        $this->assertFalse($drawing->getWidth() == 22.20);
        $this->assertFalse($drawing->getHeight() == 22.20);
        $this->assertFalse($drawing->getSelling() === false);
        $this->assertFalse($drawing->getCreationDate() === new DateTime());
        $this->assertFalse($drawing->getCreatedAt() === new DateTimeImmutable());
        $this->assertFalse($drawing->getDescription() === 'false');
        $this->assertFalse($drawing->getPortfolio() === false);
        $this->assertFalse($drawing->getSlug() === 'false');
        $this->assertFalse($drawing->getFile() === 'false');
        $this->assertNotContains(new Categorie(), $drawing->getCategorie());
        $this->assertFalse($drawing->getPrice() == 22.20);
        $this->assertFalse($drawing->getUser() === new User());
    }

    public function testIsEmpty()
    {
        $drawing = new Drawing();

        $this->assertEmpty($drawing->getName());
        $this->assertEmpty($drawing->getWidth());
        $this->assertEmpty($drawing->getHeight());
        $this->assertEmpty($drawing->getSelling());
        $this->assertEmpty($drawing->getCreationDate());
        $this->assertEmpty($drawing->getCreatedAt());
        $this->assertEmpty($drawing->getDescription());
        $this->assertEmpty($drawing->getPortfolio());
        $this->assertEmpty($drawing->getSlug());
        $this->assertEmpty($drawing->getFile());
        $this->assertEmpty($drawing->getCategorie());
        $this->assertEmpty($drawing->getPrice());
        $this->assertEmpty($drawing->getUser());
    }
}
