<?php

namespace App\Tests;

use App\Entity\Blogpost;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class BlogpostUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $blogpost = new Blogpost();
        $dateTimeImmutable = new DateTimeImmutable();

        $blogpost->setTitle('Title')
            ->setCreatedAt($dateTimeImmutable)
            ->setContent('content')
            ->setSlug('slug');

        $this->assertTrue($blogpost->getTitle() === 'Title');
        $this->assertTrue($blogpost->getCreatedAt() === $dateTimeImmutable);
        $this->assertTrue($blogpost->getContent() === 'content');
        $this->assertTrue($blogpost->getSlug() === 'slug');
    }

    public function testIsFalse()
    {
        $blogpost = new Blogpost();
        $dateTimeImmutable = new DateTimeImmutable();

        $blogpost->setTitle('Title')
            ->setCreatedAt($dateTimeImmutable)
            ->setContent('content')
            ->setSlug('slug');

        $this->assertFalse($blogpost->getTitle() === 'false');
        $this->assertFalse($blogpost->getCreatedAt() === new DateTimeImmutable);
        $this->assertFalse($blogpost->getContent() === 'false');
        $this->assertFalse($blogpost->getSlug() === 'false');
    }

    public function testIsEmpty()
    {
        $blogpost = new Blogpost();

        $this->assertEmpty($blogpost->getTitle());
        $this->assertEmpty($blogpost->getCreatedAt());
        $this->assertEmpty($blogpost->getContent());
        $this->assertEmpty($blogpost->getSlug());
    }
}
