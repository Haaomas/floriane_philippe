<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $user = new User();

        //Si je set true@test.com dans mon email 
        $user->setEmail('true@test.com')
            ->setFirstName('first name')
            ->setLastName('last name')
            ->setPassword('password')
            ->setAboutUs('about us')
            ->setInstagram('instagram');

        //Je vérifie que ce soit bien le même mail que je retrouve dans mon get
        $this->assertTrue($user->getEmail() === 'true@test.com');
        $this->assertTrue($user->getFirstName() === 'first name');
        $this->assertTrue($user->getLastName() === 'last name');
        $this->assertTrue($user->getPassword() === 'password');
        $this->assertTrue($user->getAboutUs() === 'about us');
        $this->assertTrue($user->getInstagram() === 'instagram');
    }


    public function testIsFalse()
    {
        $user = new User();

        //Si je set true@test.com
        $user->setEmail('true@test.com')
            ->setFirstName('first name')
            ->setLastName('last name')
            ->setPassword('password')
            ->setAboutUs('about us')
            ->setInstagram('instagram');

        //Et que je le compare à quelque chose de différents il va bien me retourner false
        $this->assertFalse($user->getEmail() === 'false@test.com');
        $this->assertFalse($user->getFirstName() === 'false');
        $this->assertFalse($user->getLastName() === 'false');
        $this->assertFalse($user->getPassword() === 'false');
        $this->assertFalse($user->getAboutUs() === 'false');
        $this->assertFalse($user->getInstagram() === 'false');
    }


    public function testIsEmpty()
    {
        $user = new User();

        //Si je ne set rien
        //Vérifie que je ne récupère rien
        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getFirstName());
        $this->assertEmpty($user->getLastName());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getAboutUs());
        $this->assertEmpty($user->getInstagram());
    }
}
