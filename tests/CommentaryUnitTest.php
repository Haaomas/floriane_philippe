<?php

namespace App\Tests;

use App\Entity\Commentary;
use App\Entity\Drawing;
use App\Entity\Blogpost;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class CommentaryUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $commentary = new Commentary();
        $dateTimeImmutable = new DateTimeImmutable();
        $blogpost = new Blogpost();
        $drawing = new Drawing();


        $commentary->setAuthor('author')
            ->setEmail('email@test.com')
            ->setCreatedAt($dateTimeImmutable)
            ->setContent('content')
            ->setBlogpost($blogpost)
            ->setDrawing($drawing);

        $this->assertTrue($commentary->getAuthor() === 'author');
        $this->assertTrue($commentary->getEmail() === 'email@test.com');
        $this->assertTrue($commentary->getCreatedAt() === $dateTimeImmutable);
        $this->assertTrue($commentary->getContent() === 'content');
        $this->assertTrue($commentary->getBlogpost() === $blogpost);
        $this->assertTrue($commentary->getDrawing() === $drawing);
    }

    public function testIsFalse()
    {
        $commentary = new Commentary();
        $dateTimeImmutable = new DateTimeImmutable();
        $blogpost = new Blogpost();
        $drawing = new Drawing();


        $commentary->setAuthor('false')
            ->setEmail('false@test.com')
            ->setCreatedAt(new DateTimeImmutable)
            ->setContent('false')
            ->setBlogpost(new Blogpost)
            ->setDrawing(new Drawing);

        $this->assertFalse($commentary->getAuthor() === 'author');
        $this->assertFalse($commentary->getEmail() === 'email');
        $this->assertFalse($commentary->getCreatedAt() === $dateTimeImmutable);
        $this->assertFalse($commentary->getContent() === 'content');
        $this->assertFalse($commentary->getBlogpost() === $blogpost);
        $this->assertFalse($commentary->getDrawing() === $drawing);
    }

    public function testIsEmpty()
    {
        $commentary = new Commentary();

        $this->assertEmpty($commentary->getAuthor());
        $this->assertEmpty($commentary->getEmail());
        $this->assertEmpty($commentary->getCreatedAt());
        $this->assertEmpty($commentary->getContent());
        $this->assertEmpty($commentary->getBlogpost());
        $this->assertEmpty($commentary->getDrawing());
    }
}
